function G = triPaintGouraud(F, V, C)
% Paints a tringle on the image
%
% Parameters:
%   - F: The initial image                      [Array<double> NxMx3]
%   - V: The triangle vertices                  [Matrix<double>  3x2]
%   - C: The colors for each triangle vertex    [Matrix<double>  3x3]
%
% Returns:
%   The new image                               [Array<double> NxMx3]

%% Type checks
if ~(isa(F, 'double') && length(size(F)) == 3 && size(F,3) == 3)
    throw(MException('Graphics:IllegalArgument', ...
                     'X should be a MxNx3 double array.'))
end
    
if ~(isa(V, 'double') && ismatrix(V) && isequal(size(V), [3 2]))
    throw(MException('Graphics:IllegalArgument', ...
                     'V should be a 3x2 double matrix.'))
end
    
if ~(isa(C, 'double') && ismatrix(C) && isequal(size(C), [3 3]))
    throw(MException('Graphics:IllegalArgument', ...
                     'C should be a 3x3 double matrix.'))
end
    
%% Precomputations
% Sort the triangle vertices by increasing y
[V, I] = sortrows(V,2);

% Rearange colors to match the new V matrix
C = C(I, :);

% Indices of all the edges
EI = [(1:3)' circshift(1:3, 1)'];

% All the edges of the shape
% E = [AC BA CB]
E = [V(EI(:, 1), :) V(EI(:, 2), :)];

% Y = MX + B
% M = [M(AB) M(BA) M(CB)]
M = (E(:,2) - E(:,4))./(E(:,1) - E(:,3));
% B = [B(AB) B(BA) B(CB)]
B = (E(:,4).*E(:,1) -  E(:,2).*E(:,3))./(E(:,1) - E(:,3));

% Pre-Allocate for efficiency
X  = NaN(2, V(3,2)-V(1,2)+1);
CX = NaN(2, size(X, 2), 3);

%% Lower two edges
% Y range for the scan lines [Ymin, Yintermediate)
Y = V(1,2):V(2,2)-1;
if ~isempty(Y)
    % Mapped columns on X matrix
    J = Y-V(1,2)+1;
    % Activated Edge Indices
    % AEI = [IDX(AC), IDX(BA)]
    AEI = [1 2];
    % Vertical Trace
    VT = isinf(M(AEI));
    if all(VT)
        % Vertical-Line Case
        X(:, J) = E(AEI(1), 1)*ones(2,size(Y,2));
    elseif any(VT)
        % Vertical-Edge Case
        % Rearange Activated Edge Indices
        AEI = AEI([find(VT,1) find(~VT,1)]);
        % Find Active Points
        X(:, J) = [ E(AEI(1), 1)*ones(1,size(Y,2)); ...
                    round((Y - B(AEI(2))) ./ M(AEI(2))) ];
    else
        % Normal Case
        % Y = MX + B <=> X = Y/M - B/M
        X(:, J) = round((Y - B(AEI)) ./ M(AEI));
    end
    % Interpolate for all y of both Activated Edges
    AB = E(AEI, [2 4]);
    VAB = reshape(C(EI(AEI, :),:), [2 2 3]);
    CX(:,J,:) = interpolate(AB, VAB, Y);
end

%% Critical case of 3 edged system
y = V(2,2);
% Mapped column on X matrix
j = y-V(1,2)+1;
if sum(M == 0) > 1
    % Horizontal-Line-Tringle case
    % (x,y) = (y,x) [Making the line vertical]
    V = flip(V,2);
    % Sort per old x
    [V, I] = sortrows(V,2);
    % Translate changes to color matrix
    C = C(I,:);
    % Define the range of change to the F matrix
    X = V(1,2):V(3,2);
    % Translate the triangle to the start of the canvas
    V = V - V(1,:) + 1;
    % Rerun the algorithm on a vertical line tringle
    F(y, X,:) = triPaintGouraud(ones(V(3,2)-V(1,2)+1,1,3), V, C);
    G = F;
    return;
elseif all(isnan(M))
    % Single-Point case
    F(y,V(1,1),:) = mean(C);
    G = F;
    return;
else
    if isinf(M(1))
        % Vertical AC Case
        X(:,j) = [V(2,1) V(3,1)];
    else 
        % Normal Case
        X(:,j) = [V(2,1) round((y - B(1))/M(1))];
    end
    VAB = [C(3,:); C(1,:)];
    VAB = permute(VAB, [3 1 2]);
    CX(:,j,:) = [C(2,:); interpolate([V(3,2) V(1,2)], VAB, y)];
end

%% Upper two edges
% Y range for the scan lines (Yintermediate, Ymax]
Y = V(2,2)+1:V(3,2);
if ~isempty(Y)
    % Mapped columns on X matrix
    J = Y-V(1,2)+1;
    % Activated Edge Indices
    % AEI = [IDX(AC), IDX(CB)]
    AEI = [1 3];
    % Vertical Trace
    VT = isinf(M(AEI));
    if all(VT)
        % Vertical-Line Case
        X(:, J) = E(AEI(1), 1)*ones(2,size(Y,2));
    elseif any(VT)
        % Vertical-Edge Case
        % Update Activated Edge Indices
        AEI = AEI([find(VT,1) find(~VT,1)]);
        % Find Active Points
        X(:, J) = [ E(AEI(1), 1)*ones(1,size(Y,2)); ...
                    round((Y - B(AEI(2))) ./ M(AEI(2))) ];
    else
        % Normal Case
        % Y = MX + B <=> X = Y/M - B/M
        X(:, J) = round((Y - B(AEI)) ./ M(AEI));
    end
    % Interpolate for all y of both Activated Edges
    AB = E(AEI, [2 4]);
    VAB = reshape(C(EI(AEI, :),:), [2 2 3]);
    CX(:,J,:) = interpolate(AB, VAB, Y);
end

% Column-wise min/max
I = (X(1,:) > X(2,:));
X(:,I) = flip(X(:,I));

% Translate Changes to color matrix as well
CX(:,I,:) = flip(CX(:,I,:));
% Rearange dimensions to match interpolate function
CX = permute(CX, [2 1 3]);

%% Drawing
% Draw pixels
for y = V(1,2):V(3,2)
    j = y - V(1,2) + 1;
    F(y, X(1,j):X(2,j), :) = interpolate(X(:,j)', CX(j,:,:), X(1,j):X(2,j));
end

G = F;

end