function V = interpolate(AB, VAB, P)
% Interpolates P to AB to find the correct V
%
% Parameters:
%   - AB:  The bounds of the interpolation      [Matrix<double>  Kx2  ]
%   - VAB: The values for each boundary         [Array<double>   Kx2xM]
%   - P:   The values to interpolate            [Vector<double>  1xN  ]
%
% Returns:
%   The new values                              [Array<double>  KxNxM ]

%% Type checks
if ~(isa(AB, 'double') && ismatrix(AB) && size(AB, 2) == 2)
    throw(MException('Graphics:IllegalArgument', ...
                     'AB should be a Kx2 double matrix.'))
end

SZ = size(VAB);
if ~(isa(VAB, 'double') && length(SZ) >= 2 && isequal(SZ([1 2]), size(AB)))
    throw(MException('Graphics:IllegalArgument', ...
            sprintf('VAB should be a %dx2xM double matrix.', size(AB,1))))
end

if ~(isa(P, 'double') && isvector(P) && size(P,1) == 1)
    throw(MException('Graphics:IllegalArgument', ...
                     'P should be a 1xN double vector.'))
end

%% Preset variables
I = (AB(:,1) ~= AB(:,2));
A = AB(I,1);
B = AB(I,2);

VA = VAB(I, 1, :);
VB = VAB(I, 2, :);

%% Code
% Calculate coefficients for each interpolation
L = (P - B)./(A - B);
% Calculate the value after the interpolation
V(I,:,:) = L.*VA + (1-L).*VB;
% If there are overlapping bounds assume value is the mean
V(~I,:,:) = repmat(mean(VAB(~I,:,:), 2), [1 length(P)]);

end

