function I = paintObject(V, F, C, D, painter)
% Paints an object on a new canvas
%
% Parameters:
%   - V: All vertices                     [Matrix<double>      Kx2]
%   - F: Indices that define a triangle   [Matrix<double>      Nx3]
%   - C: The colors for each vertex       [Matrix<double>      Kx3]
%   - D: The depth for each vertex        [Vector<double>      Kx1]
%   - painter: Selects the painter        [String 'flat'/'gouraud']
%
% Returns:
%   The new image                         [Array<double> 1200x1200x3]

%% Type checks
VSZ = size(V);
if ~(isa(V, 'double') && ismatrix(V) && VSZ(2) == 2)
    throw(MException('Graphics:IllegalArgument', ...
                     'V should be a Kx2 double matrix.'))
end
    
if ~(isa(F, 'double') && ismatrix(F) && size(F,2) == 3)
    throw(MException('Graphics:IllegalArgument', ...
                     'F should be a Nx3 double matrix.'))
end
    
if ~(isa(C, 'double') && ismatrix(C) && isequal(size(C), [VSZ(1) 3]))
    throw(MException('Graphics:IllegalArgument', ...
                     'C should be a Kx3 double matrix.'))
end

if ~(isa(D, 'double') && isvector(D) && isequal(size(D), [VSZ(1) 1]))
    throw(MException('Graphics:IllegalArgument', ...
                     'D should be a Kx1 double vector.'))
end

if ~strcmp(painter, 'flat') && ~strcmp(painter, 'gouraud')
throw(MException('Graphics:IllegalArgument', ...
                 'Painter should be ''flat'' or ''gouraud''.'))
end

%% Code
% Create new canvas
I = ones(1200, 1200, 3);

% Select all vertices, their colors and depths for each tringle
TRI = reshape(V(F, :), [size(F) 2]);
COL = reshape(C(F, :), [size(F) 3]);
DPT = mean(D(F), 2);

% Sort depth vector and translate changes to TRI and COL
[~, IDX] = sort(DPT, 'descend');
TRI = TRI(IDX, :, :);
COL = COL(IDX, :, :);


% Paint all tringles
for i = 1:size(TRI, 1)
    % Reshape to match acceptable dimensions
    V = reshape(TRI(i, :, :), [3 2]);
    C = reshape(COL(i, :, :), [3 3]);
 
    % Select method to paint
    if painter == "flat"
        I = triPaintFlat(I, V, C);
    elseif painter == "gouraud"
        I = triPaintGouraud(I, V, C);
    end
 
end

end