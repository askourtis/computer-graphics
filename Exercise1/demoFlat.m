% Clear all
clear classes
% Constants
painter    = 'flat';
read_file  = 'duck_hw1.mat';
write_file = sprintf('%s_duck.png', painter);
% Load the data
disp('Loading data...')
load(read_file)
% Run the algorithm and save to file
disp('Processing...')
imwrite(paintObject(V_2d, F, C, D, painter), write_file)
fprintf('Image written on: %s\n', write_file);